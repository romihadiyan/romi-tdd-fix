from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Tdd_Form
from .models import TddModel

# Create your views here.

response = {}

def index(request):
    response["listform"]= Tdd_Form
    myform= TddModel.objects.all()
    response["Tdd_Form"]= myform

    form= Tdd_Form(request.POST or None)
    if(request.method == "POST" and form.is_valid()):
        response['my_status'] = request.POST['my_status']
        myform  = TddModel(my_status = response['my_status'])
        myform.save()
        return render(request, 'tdd.html', response)
    else:
        return render(request, 'tdd.html', response)
                           
                        
