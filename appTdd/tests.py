from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.test import TestCase, LiveServerTestCase
from .views import index
from .models import TddModel
from .forms import Tdd_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time


# Create your tests here.

class Lab6UnitTest(TestCase):
    def test_story_6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_story6_contain_string(self):
        request= HttpRequest()
        response= index(request)
        string = response.content.decode("utf8")
        self.assertIn("Hello, Apa Kabar?", string)

    def test_story6_using_template(self):
        response = Client().get("/")
        self.assertTemplateUsed(response, "tdd.html")


    def test_story6_has_hello(self):
        response = Client().get('/')
        hello = response.content.decode('utf-8')
        self.assertIn("Hey guys my name is Romi Hadiyan, i'm just an ordinary student who has interest in information system.", hello)


# class myFunctionalTest(LiveServerTestCase):

#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.selenium.implicitly_wait(25) 
#         super(myFunctionalTest,self).setUp()


#     def tearDown(self):
#         self.selenium.quit()
#         super(myFunctionalTest, self).tearDown()


#     def test_input_todo_lab7(self):
#         selenium= webdriver.Chrome()
#         selenium.get(self.live_server_url)
#         my_status=selenium.find_element_by_class_name('form-control')
#         submit=selenium.find_element_by_id('button')

#         my_status.send_keys('Coba coba')

#         submit.send_keys(Keys.RETURN)
#         time.sleep(5)


#     def test_position(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url)
#         title = selenium.find_element_by_tag_name("h1")
#         button = selenium.find_element_by_id("button")
#         self.assertEqual(title.location, {'x': 0, 'y': 20})
#         self.assertEqual(button.location, {'x': 0, 'y': 220})
        

#     def test_css(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url)
#         title = selenium.find_element_by_tag_name("h1")
#         button = selenium.find_element_by_id("button")
#         self.assertEqual(title.value_of_css_property("font-size"), "36px")
#         self.assertEqual(button.value_of_css_property("font-size"), "14px")
        
        