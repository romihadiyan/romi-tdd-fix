from django.contrib import admin
from django.urls import path
from story9 import views
from django.conf import settings
from django.conf.urls import url, include
from django.contrib.auth import views
from .views import login
from .views import data



urlpatterns = [
    path('', login, name='story9'),
    path('data/<slug:text>',data, name='data'),
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    url(r'^auth/', include('social_django.urls', namespace='social')),
    
]
