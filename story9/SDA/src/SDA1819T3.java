import java.io.*;
import java.util.*;

/**
 * MAIN SOURCE FOR THIS TP : GEEKS FOR GEEKS
 * https://www.geeksforgeeks.org/avl-tree-set-1-insertion/
 * https://www.geeksforgeeks.org/avl-tree-set-2-deletion/
 * https://algs4.cs.princeton.edu/code/edu/princeton/cs/algs4/AVLTreeST.java.html
 * https://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/
 * https://www.geeksforgeeks.org/find-distance-between-two-nodes-of-a-binary-tree/
 */



public class SDA1819T3 {
    static int q;
    static int k;
    static String x;

    public static void main(String[] args) throws IOException {
        AVLTree tree = new AVLTree();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        q = Integer.parseInt(reader.readLine());
        for(int i =0; i<q ; i++){
            String[] control = reader.readLine().split(" ");
            if (control[0].equals("GABUNG")){
                if(control.length ==2){
                    tree.insert(control[1]);
                }
                else{
                    k=Integer.parseInt(control[2]);
                    //memeriksa apakah bisa di insert
                }
            }

            else if(control[0].equals("PARTNER")){
                System.out.println(tree.searchPartner(control[1]));
            }

            else if (control[0].equals("MENYERAH")){
                tree.menyerah(control[1]);
            }

            else if (control[0].equals("PRINT")){
                tree.preOrder(tree.root);
                System.out.println();
                tree.postOrder(tree.root);
                System.out.println();

            }

            else if (control[0].equals("MUSIBAH")){
                tree.musibah();
                System.out.println();
            }

            else if (control[0].equals("PANJANG-TALI")) {
                System.out.println(tree.findDistance(tree.root, control[1], control[2]));

            }

            else if (control[0].equals("GANTUNGAN")){
                int N = Integer.parseInt(control[1]);
                Node iniNode = tree.gantungan(control[2],control[3]);
                for(int k = 0; k<N-1; k++){
                    iniNode = tree.gantungan(iniNode.key, control[3+k]);

                }
                System.out.println(iniNode.key);

            }

            else if (control[0].equals("CEDERA")){
                tree.cedera(control[1]);
            }


        }

    }


}

class Node {
    String key;
    int height;
    Node left, right;

    Node(String d){
        key = d;
        height=1;
    }
}

class AVLTree{
    Node root;

    //Funsi untuk mendapatkan tinggi dari tree tersebut
    int height(Node N){
        if(N == null){
            return 0;
        }
        return N.height;
    }

    //fungsi untuk mendapatkan maksimum dari dua integer
    int max(int a, int b){
        return (a > b) ? a:b ;
    }

    //fungsi untuk right rotasi subtree yang ter root dengan y
    Node rightRotate(Node y){
        Node x = y.left;
        Node T2 = x.right;

        //melakukan rotasi
        x.right = y;
        y.left = T2;

        //update heights
        y.height = max(height(y.left), height(y.right))+1;
        x.height = max(height(x.left), height(x.right))+1;

        //Return new root
        return x;
    }

    int getBalance(Node N){
        if (N == null){
            return 0;
        }
        return height(N.left) - height(N.right);

    }

    Node leftRotate(Node x){
        Node y = x.right;
        Node T2 = y.left;

        //melakukan rotasi left
        y.left = x;
        x.right = T2;

        //update heights
        x.height = max(height(x.left), height(x.right)) + 1;
        y.height = max(height(y.left), height(y.right)) + 1;

        // Return new root
        return y;
    }

    void insert(String key){
        this.root = insert(this.root, key);
    }

    Node insert(Node node, String key){
        // 1. melakukan insert BST normal
        if(node == null){
            return (new Node(key));
        }

        if(key.compareTo(node.key)<0){
            node.left = insert(node.left, key);

        }
        else if (key.compareTo(node.key)>0){
            node.right = insert(node.right, key);

        }

        else{ //kondisi saat duplikasi key tidak diizinkan{
            return node;
        }
        //2. mengupdate tinggi dari node ancestor
        node.height = 1+max(height(node.left), height(node.right));

        //3. mendapatkan balance faktor dari ancestor node untuk cek apakah node menjadi tidak seimbang left left case
        int balance = getBalance(node);

        //jika node menjadi tidak seimbang, maka ada 4 case

        if(balance > 1 && key.compareTo(node.left.key)<0) {
            return rightRotate(node);
        }
        //right right case
        if (balance < -1 && key.compareTo(node.right.key)>0) {
            return leftRotate(node);
        }
        //left right case
        if(balance > 1 && key.compareTo(node.left.key)>0){
            node.left = leftRotate(node.left);
            return rightRotate(node);
        }

        //right left case
        if(balance < -1 && key.compareTo(node.right.key)<0){
            node.right = rightRotate(node.right);
            return leftRotate(node);
        }

        //mengembalikan node pointer yang tidak berubah
        return node;






    }

    String searchPartner(String key){
        return searchPartner(this.root, key);
    }

    //method fungsi untuk mencari partner dari key
    String searchPartner(Node root, String key){
        if (root == null || this.root.key.equals(key)){
            return "TIDAK ADA";
        }

        if(key.compareTo(root.key)<0){
            if (root.left.key.equals(key)){
                if(root.right == null){
                    return "TIDAK ADA";
                }
                return root.right.key;
            }
            return searchPartner(root.left,key);
        }

        //kondisi ketika val lebih besar dari key root
        else{
            if (root.right.key.equals(key)){
                if(root.left == null){
                    return"TIDAK ADA";
                }
                return root.left.key;
            }
            return searchPartner(root.right,key);
        }
    }

    void preOrder(Node node){
        if(node == null){
            return;
        }

        //first print data of node
        System.out.print(node.key+";");

        //recur on left subtree
        preOrder(node.left);

        //recur on right subtree
        preOrder(node.right);
    }

    void postOrder(Node node){
        if(node==null){
            return;
        }

        //first recur on left subtree
        postOrder(node.left);

        //then recur on right subtree
        postOrder(node.right);

        //now deal with the node
        System.out.print(node.key+";");
    }

    Node musibah(Node current){
        if(this.root == null){
            return null;
        }

        if(current == null){
            return null;
        }

        if (current.left== null && current.right==null){
            System.out.print(current.key+";");
            return null;
        }

        current.left = musibah(current.left);
        current.right=musibah(current.right);
        return current;

    }

    void musibah() {
        this.root = musibah(this.root);
    }

    Node minValueNode(Node node){
        Node current = node;
        // loop down to find the leftmost leave
        while (current.left!=null){
            current = current.left;
        }
        return current;
    }

    Node maxValueNode(Node node) {
        Node current = node;

        /* loop down to find the rightmost leaf */
        while (current.right != null)
            current = current.right;

        return current;
    }

    void cedera(String key){
        this.root = cedera(this.root, key);
    }

    Node cedera(Node root, String key){
        // STEP 1: PERFORM STANDARD BST DELETE
        if (root == null)
            return root;

        // If the key to be deleted is smaller than
        // the root's key, then it lies in left subtree
        if (key.compareTo(root.key)<0)
            root.left = cedera(root.left, key);

            // If the key to be deleted is greater than the
            // root's key, then it lies in right subtree
        else if (key.compareTo(root.key)>0)
            root.right = cedera(root.right, key);

            // if key is same as root's key, then this is the node
            // to be deleted
        else
        {

            // node with only one child or no child
            if ((root.left == null) || (root.right == null))
            {
                Node temp = null;
                if (temp == root.left)
                    temp = root.right;
                else
                    temp = root.left;

                // No child case
                if (temp == null)
                {
                    temp = root;
                    root = null;
                }
                else // One child case
                    root = temp; // Copy the contents of
                // the non-empty child
            }
            else
            {

                // node with two children: Get the inorder
                // successor (smallest in the right subtree)
                Node temp = minValueNode(root.right);

                // Copy the inorder successor's data to this node
                root.key = temp.key;

                // Delete the inorder successor
                root.right = cedera(root.right, temp.key);
            }
        }

        // If the tree had only one node then return
        if (root == null)
            return root;

        // STEP 2: UPDATE HEIGHT OF THE CURRENT NODE
        root.height = max(height(root.left), height(root.right)) + 1;

        // STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to check whether
        // this node became unbalanced)
        int balance = getBalance(root);

        // If this node becomes unbalanced, then there are 4 cases
        // Left Left Case
        if (balance > 1 && getBalance(root.left) >= 0)
            return rightRotate(root);

        // Left Right Case
        if (balance > 1 && getBalance(root.left) < 0)
        {
            root.left = leftRotate(root.left);
            return rightRotate(root);
        }

        // Right Right Case
        if (balance < -1 && getBalance(root.right) <= 0)
            return leftRotate(root);

        // Right Left Case
        if (balance < -1 && getBalance(root.right) > 0)
        {
            root.right = rightRotate(root.right);
            return leftRotate(root);
        }

        return root;

    }

    void printGivenLevel (Node root ,int level)
    {
        if (root == null)
            return;
        if (level == 1)
            System.out.print(root.key + ";");
        else if (level > 1)
        {
            printGivenLevel(root.left, level-1);
            printGivenLevel(root.right, level-1);
        }
    }

    void printLevelOrder (){
        int h = height(root);
        int i;
        for (i=1; i<=h; i++){
            printGivenLevel(root, i);
        }
    }

    void menyerah(String key){
        this.root = menyerah(this.root, key);
    }


    Node menyerah(Node root, String key){
        // STEP 1: PERFORM STANDARD BST DELETE
        if (root == null)
            return root;

        // If the key to be deleted is smaller than
        // the root's key, then it lies in left subtree
        if (key.compareTo(root.key) < 0)
            root.left = menyerah(root.left, key);

            // If the key to be deleted is greater than the
            // root's key, then it lies in right subtree
        else if (key.compareTo(root.key) > 0)
            root.right = menyerah(root.right, key);

            // if key is same as root's key, then this is the node
            // to be deleted
        else {

            // node with only one child or no child
            if ((root.left == null) || (root.right == null)) {
                Node temp = null;
                if (temp == root.left)
                    temp = root.right;
                else
                    temp = root.left;

                // No child case
                if (temp == null) {
                    temp = root;
                    root = null;
                } else // One child case
                    root = temp; // Copy the contents of
                // the non-empty child
            } else {

                // node with two children: Get the inorder
                // successor (smallest in the right subtree)
                Node temp = maxValueNode(root.left);

                // Copy the inorder successor's data to this node
                root.key = temp.key;

                // Delete the inorder successor
                root.left = menyerah(root.left, temp.key);
            }
        }

        // If the tree had only one node then return
        if (root == null)
            return root;

        // STEP 2: UPDATE HEIGHT OF THE CURRENT NODE
        root.height = max(height(root.left), height(root.right)) + 1;

        // STEP 3: GET THE BALANCE FACTOR OF THIS NODE (to check whether
        // this node became unbalanced)
        int balance = getBalance(root);

        // If this node becomes unbalanced, then there are 4 cases
        // Left Left Case
        if (balance > 1 && getBalance(root.left) >= 0)
            return rightRotate(root);

        // Left Right Case
        if (balance > 1 && getBalance(root.left) < 0) {
            root.left = leftRotate(root.left);
            return rightRotate(root);
        }

        // Right Right Case
        if (balance < -1 && getBalance(root.right) <= 0)
            return leftRotate(root);

        // Right Left Case
        if (balance < -1 && getBalance(root.right) > 0) {
            root.right = rightRotate(root.right);
            return leftRotate(root);
        }

        return root;
    }

    Node findNode(Node temp, String key){
        if(temp.key.compareTo(key)==0){
            return temp;
        }

        if (temp.key.compareTo(key)<0){
            return findNode(temp.right, key);
        }

        return findNode(temp.left,key);
    }

    // method gantungan similar to LCM
    Node gantungan(String first, String second){
        return gantungan(this.root,first,second);
    }
    Node gantungan(Node n, String n1, String n2){
        if(n==null)return null;

        if(n.key.compareTo(n1)>0 && n.key.compareTo(n2)>0){
            return gantungan(n.left,n1,n2);
        }
        if(n.key.compareTo(n1)<0 && n.key.compareTo(n2)<0){
            return gantungan(n.right,n1,n2);
        }
        return n;
        //seperti itu
    }

    int findDistance(Node root, String a, String b) {
        Node lca = LCA(root, a, b);

        int d1 = findLevel(lca, a, 0);
        int d2 = findLevel(lca, b, 0);

        return d1 + d2;
    }

    private int findLevel(Node root, String a, int level) {
        if (root == null)
            return -1;
        if (root.key.equals(a))
            return level;
        int left = findLevel(root.left, a, level + 1);
        if (left == -1)
            return findLevel(root.right, a, level + 1);
        return left;
    }

    private Node LCA(Node root, String n1, String n2) {
        if (root == null)
            return root;
        if (root.key.equals(n1) || root.key.equals(n2)) {
            return root;
        }

        Node left = LCA(root.left, n1, n2);
        Node right = LCA(root.right, n1, n2);

        if (left != null && right != null)
            return root;
        if (left != null)
            return LCA(root.left, n1, n2);
        else
            return LCA(root.right, n1, n2);
    }
















}