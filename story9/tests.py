from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import login
import unittest
import time

# Create your tests here.
class Story9_Test(TestCase):
    def test_story_9_url_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)


    def test_story9_using_favbook_template(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'fave.html')

    def test_story9_using_index_func(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, login)
    
    def test_story9_index_header(self):
        request = HttpRequest()
        response = login(request)
        html_response = response.content.decode('utf8')
        self.assertIn('List of the Books', html_response)

