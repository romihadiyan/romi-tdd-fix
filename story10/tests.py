from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.apps import apps
from django.utils.encoding import force_text
from .models import Subscriber
import unittest





class Lab9UnitTest(TestCase): 
           
    def test_library_url_is_exist(self):
        response = Client().get('/story10/subscribe')
        self.assertEqual(response.status_code, 200)

    

    def test_library_using_library_template(self):
        response = Client().get('/story10/subscribe')
        self.assertTemplateUsed(response, 'form_page.html')


    def test_model_can_create_new_subscriber(self):
        new_subscriber = Subscriber.objects.create(name="me", email="itsme@yahoo.com", password="hello1234")
        counting_all_subscriber = Subscriber.objects.all().count()
        self.assertEqual(counting_all_subscriber, 1)
    
    def test_max_length_name(self):
        name = Subscriber.objects.create(name="paling banyak 30 karakter ya")
        self.assertLessEqual(len(str(name)), 30)

    def test_check_email_view_get_return_200(self):
        email = "asti@gmail.com"
        Client().post('/story10/check_email', {'email': email})
        response = Client().post('/story10/subscribe', {'email': 'asti@gmail.com'})
        self.assertEqual(response.status_code, 200)



