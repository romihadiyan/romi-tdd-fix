from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import JsonResponse
import urllib.request, json
import requests
from .forms import * 
from .models import * 
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
# gatauuuuuuuuZZ
from django.core import serializers
from django.db import IntegrityError
from django.views.decorators.csrf import csrf_exempt


response={}

def subscribe(request):
    response = {
        'subscribe_form' : Project_Form
    }
    context={'subscribe':'active'}
    html = 'form_page.html'
    return render(request, html, response)
    

def check_email(request):
    try:
        print(request.POST['email'])
        validate_email(request.POST['email'])
    except:
        return JsonResponse({
            'message':'Email format is wrong!',
            'status':'fail'
        })

    exist = Subscriber.objects.filter(email=request.POST['email'])

    if exist:
        return JsonResponse({
            'message':'ups, e-mail already exist',
            'status':'fail'
        })
    
    return JsonResponse({
        'message':'Email can be used',
        'status':'success'
    })

def save_subscriber(request):
    if (request.method == "POST"):
        subscriber = Subscriber(
            email = request.POST['email'],
            name = request.POST['name'],
            password = request.POST['password']   
        )
        subscriber.save()
        
        return JsonResponse({
            'message':'Subscribe success! We are friends now!'
        }, status = 200)
    else:
        return JsonResponse({
            'message':"There's no GET method here!"
        }, status = 403)


def listSubscriber(request):
    all_subs = Subscriber.objects.all().values()
    subs = list(all_subs)
    return JsonResponse({'all_subs' : subs})


@csrf_exempt
def unsubscribe(request):
    if request.method == 'POST':
        email = request.POST['email']
        Subscriber.objects.get(email=email).delete()
        return redirect('subscribe')

