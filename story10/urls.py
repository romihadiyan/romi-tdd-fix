from django.urls import path
from .views import *

urlpatterns = [
    path('subscribe', subscribe, name='subscribe'),
    path('check_email/', check_email, name='check_email'),
    path('save_subscriber/', save_subscriber, name='save_subscriber'),
    path('list-subscriber/', listSubscriber, name='listSubscriber'),
    path('unsubscribe/', unsubscribe, name='unsubscribe'),

]
